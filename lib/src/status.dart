import "mark.dart";

class Status {

    _Flag _flag;
    Mark _winner;

    Status.ongoing() {
        _flag = _Flag.ONGOING;
    }

    Status.ended(Mark winner) {
        _flag = _Flag.ENDED;
        _winner = winner;
    }

    bool isOngoing() {
        return _flag == _Flag.ONGOING;
    }

    bool isEnded() {
        return _flag == _Flag.ENDED;
    }

    Mark getWinner() {
        return _winner;
    }

}

enum _Flag {
    ONGOING, ENDED
}