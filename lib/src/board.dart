import "mark.dart";
import "status.dart";
import "turn.dart";

class Board {

    static const int NUMBER_OF_COLUMNS = 3;
    static const int NUMBER_OF_ROWS = 3;

    Turn _turn;
    List<Mark> _grid;
    Status _status;

    Board(Turn turn) {
        _turn = turn;
        _grid = List(NUMBER_OF_COLUMNS * NUMBER_OF_ROWS);
        _status = Status.ongoing();
    }

    void select(int cell) {
        _assertStatusNotEnded(_status);
        _assertValidCell(_grid, cell);
        _assertCellNotSelected(_grid, cell);

        _grid[cell] = _turn.getCurrent();
        _turn.next();

        _updateStatus();
    }

    Mark getMarkAt(int cell) {
        return _grid[cell];
    }

    Mark getCurrentTurn() {
        return _turn.getCurrent();
    }

    Status getStatus() {
        return _status;
    }

    bool isClean() {
        return _grid.every((element) => element == null);
    }

    void reset() {
        _turn.reset();
        _grid.fillRange(0, _grid.length, null);
    }

    void _updateStatus() {
        Status newStatus;

        // Check diagonal.
        if ((newStatus = _checkDiagonalWinningCondition(_grid)) != null) {
            _status = newStatus;
            return;
        }

        // Check vertical.
        if ((newStatus = _checkVerticalWinningCondition(_grid)) != null) {
            _status = newStatus;
            return;
        }

        // Check horizontal.
        if ((newStatus = _checkHorizontalWinningCondition(_grid)) != null) {
            _status = newStatus;
            return;
        }

        // Check if all grid cells are selected already.
        if ((newStatus = _checkAllCellsAreSelected(_grid)) != null) {
            _status = newStatus;
            return;
        }
    }

    static Status _checkVerticalWinningCondition(List<Mark> grid) {
        for (int c = 0; c < 2; c++) {
            if (grid[c] != null
                && grid[c] == grid[c + 3]
                && grid[c] == grid[c + 6]) {
                return Status.ended(grid[c]);
            }
        }
        return null;
    }

    static Status _checkHorizontalWinningCondition(List<Mark> grid) {
        for (int r = 0; r < grid.length; r += 3) {
            if (grid[r] != null
                && grid[r] == grid[r + 1]
                && grid[r] == grid[r + 2]) {
                return Status.ended(grid[r]);
            }
        }
        return null;
    }

    static Status _checkDiagonalWinningCondition(List<Mark> grid) {
        if (grid[0] != null
            && grid[0] == grid[4]
            && grid[0] == grid[8]) {
            return Status.ended(grid[0]);
        } else if (grid[2] != null
            && grid[2] == grid[4]
            && grid[2] == grid[6]) {
            return Status.ended(grid[2]);
        }
        return null;
    }

    static Status _checkAllCellsAreSelected(List<Mark> grid) {
        if (grid.every((mark) => mark != null)) {
            return Status.ended(null);
        }
        return null;
    }

    static void _assertStatusNotEnded(Status status) {
        if (status.isEnded()) {
            throw AlreadyEndedError();
        }
    }

    static void _assertValidCell(List<Mark> grid, int cell) {
        if (cell < 0 || cell >= grid.length) {
            throw InvalidCellError();
        }
    }

    static void _assertCellNotSelected(List<Mark> grid, int cell) {
        if (grid[cell] != null) {
            throw CellIsAlreadySelectedError();
        }
    }

}

class CellIsAlreadySelectedError extends Error {
}

class AlreadyEndedError extends Error {
}

class InvalidCellError extends Error {
}