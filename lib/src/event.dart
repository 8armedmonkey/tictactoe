abstract class EventBroadcast {

    void send(Event event);

    Subscription subscribe();

}

abstract class Event {
}

abstract class Subscriber {

    void onEvent(Event event);

}

abstract class Subscription {

    void unsubscribe();

}