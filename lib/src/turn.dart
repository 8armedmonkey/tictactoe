import "mark.dart";

class Turn {

    Mark _first;
    Mark _current;

    Turn(this._first) {
        this._current = this._first;
    }

    Mark getFirst() {
        return _first;
    }

    Mark getCurrent() {
        return _current;
    }

    void next() {
        switch (_current) {
            case Mark.X:
                _current = Mark.O;
                break;

            case Mark.O:
                _current = Mark.X;
                break;
        }
    }

    void reset() {
        _current = _first;
    }

}