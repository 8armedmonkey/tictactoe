import "package:test/test.dart";
import "package:tictactoe/tictactoe.dart";

main() {
    test("Status.isOngoing should return true if the status is 'ongoing'", () {
        expect(Status.ongoing().isOngoing(), isTrue);
    });

    test("Status.isOngoing should return false if the status is not 'ongoing'", () {
        expect(Status.ended(Mark.X).isOngoing(), isFalse);
    });

    test("Status.isEnded should return true if the status is 'ended'", () {
        expect(Status.ended(Mark.X).isEnded(), isTrue);
    });

    test("Status.isEnded should return false if the status is not 'ended'", () {
        expect(Status.ongoing().isEnded(), isFalse);
    });

    test("Status.getWinner should return null if the status is 'ongoing'", () {
        expect(Status.ongoing().getWinner(), isNull);
    });

    test("Status.getWinner should return whatever passed if the status is 'ended'", () {
        expect(Status.ended(null).getWinner(), isNull);
        expect(Status.ended(Mark.X).getWinner(), equals(Mark.X));
        expect(Status.ended(Mark.O).getWinner(), equals(Mark.O));
    });
}