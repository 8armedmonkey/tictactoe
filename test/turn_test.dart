import "package:test/test.dart";
import "package:tictactoe/tictactoe.dart";

main() {
    Turn turn;

    setUp(() {
        turn = Turn(Mark.X);
    });

    test("Turn.getFirst should return the first mark passed in constructor", () {
        expect(turn.getFirst(), equals(Mark.X));
    });

    test("Turn.next should advance to the next mark", () {
        expect(turn.getCurrent(), equals(Mark.X));

        turn.next();
        expect(turn.getCurrent(), equals(Mark.O));

        turn.next();
        expect(turn.getCurrent(), equals(Mark.X));
    });

    test("Turn.next should not advance the first mark", () {
        turn.next();
        expect(turn.getFirst(), equals(Mark.X));
    });

    test("Turn.reset should reset current to first mark", () {
        turn.next();
        turn.reset();
        expect(turn.getCurrent(), equals(turn.getFirst()));
    });
}