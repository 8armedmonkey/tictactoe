import "package:test/test.dart";
import "package:tictactoe/tictactoe.dart";

main() {
    Board board;

    setUp(() {
        board = new Board(new Turn(Mark.X));
    });

    group("Board.select", () {
        test("Should assign current mark to the specified cell", () {
            board.select(0);

            expect(board.getMarkAt(0), equals(Mark.X));
        });

        test("Should advance turn", () {
            board.select(0);
            expect(board.getCurrentTurn(), equals(Mark.O));

            board.select(1);
            expect(board.getCurrentTurn(), equals(Mark.X));
        });

        test("Should throw error when the cell is already selected", () {
            board.select(0);
            expect(() => board.select(0),
                throwsA(TypeMatcher<CellIsAlreadySelectedError>()));
        });

        test("Should throw error when the status is not 'ongoing'", () {
            board.select(0);
            board.select(1);
            board.select(3);
            board.select(4);
            board.select(6);

            expect(() => board.select(8), throwsA(TypeMatcher<AlreadyEndedError>()));
        });

        test("Should throw error when the cell is invalid", () {
            // Valid cell index is [0 .. (Board.NUMBER_OF_COLUMNS * Board.NUMBER_OF_ROWS)].
            expect(() => board.select(-1),
                throwsA(TypeMatcher<InvalidCellError>()));
            expect(() => board.select(9),
                throwsA(TypeMatcher<InvalidCellError>()));
        });
    });

    group("Board.getMarkAt", () {
        test("Should return the mark if the cell has been selected", () {
            board.select(0);

            expect(board.getMarkAt(0), equals(Mark.X));
        });

        test("Should return null if the cell has not been selected", () {
            expect(board.getMarkAt(0), isNull);
        });
    });

    group("Board.getStatus", () {
        test("Should be ongoing if the board is empty", () {
            expect(board.getStatus().isOngoing(), isTrue);
        });

        test("Should be ongoing if no end condition has been reached", () {
            board.select(0);
            board.select(1);

            expect(board.getStatus().isOngoing(), isTrue);
        });

        test("Should be ended if one side is winning", () {
            board.select(0);
            board.select(1);
            board.select(3);
            board.select(4);
            board.select(6);

            expect(board.getStatus().isEnded(), isTrue);
            expect(board.getStatus().getWinner(), equals(Mark.X));
        });

        test("Should be ended if all cells are selected already", () {
            // No winning condition but all cells are occupied.
            board.select(1);
            board.select(0);
            board.select(2);
            board.select(4);
            board.select(3);
            board.select(5);
            board.select(7);
            board.select(6);
            board.select(8);

            expect(board.getStatus().isEnded(), isTrue);
            expect(board.getStatus().getWinner(), isNull);
        });
    });

    group("Board.isClean", () {
        test("Should return true if the state is clean", () {
            expect(board.isClean(), isTrue);
        });

        test("Should return false if the state is not clean", () {
            board.select(0);

            expect(board.isClean(), isFalse);
        });
    });

    group("Board.reset", () {
        test("Should clean existing state", () {
            board.select(0);
            board.reset();

            expect(board.isClean(), isTrue);
            expect(board.getMarkAt(0), isNull);
        });

        test("Should reset the turn", () {
            board.select(0);
            board.reset();

            expect(board.getCurrentTurn(), equals(Mark.X));
        });
    });
}