import "dart:io";
import "package:tictactoe/tictactoe.dart";

class Presenter {

    int acceptInput(Mark currentTurn) {
        print("It's ${_getMarkRepresentation(currentTurn)} turn. "
            "Select which cell?");

        return int.parse(stdin.readLineSync());
    }

    void render(Board board) {
        print("");

        for (int i = 0; i < Board.NUMBER_OF_ROWS; i++) {
            print(
                "${_getCellContent(board.getMarkAt(i * 3))}"
                    "${_getColumnSeparator()}"
                    "${_getCellContent(board.getMarkAt(i * 3 + 1))}"
                    "${_getColumnSeparator()}"
                    "${_getCellContent(board.getMarkAt(i * 3 + 2))}"
            );

            if (i < Board.NUMBER_OF_ROWS - 1) {
                print("${_getRowSeparator()}");
            } else {
                print("\n");
            }
        }
    }

    void presentWinner(Mark mark) {
        if (mark != null) {
            _presentMessage("The winner is ${_getMarkRepresentation(mark)}.");
        } else {
            _presentMessage("The game is a tie.");
        }
    }

    void presentError(Error error) {
        if (error is InvalidCellError) {
            _presentError("Invalid cell index. Select within the range of "
                "[0..${Board.NUMBER_OF_ROWS * Board.NUMBER_OF_COLUMNS - 1}]");

        } else if (error is CellIsAlreadySelectedError) {
            _presentError("Cell is already selected.");

        } else if (error is AlreadyEndedError) {
            _presentError("The game has ended.");
        }
    }

    void _presentMessage(String message) {
        print("\n$message\n");
    }

    void _presentError(String message) {
        stderr.writeln("\n$message\n");
    }

    String _getRowSeparator() {
        return "---+---+---";
    }

    String _getColumnSeparator() {
        return "|";
    }

    String _getCellContent(Mark mark) {
        return " ${_getMarkRepresentation(mark)} ";
    }

    String _getMarkRepresentation(Mark mark) {
        if (mark != null) {
            switch (mark) {
                case Mark.X:
                    return "X";
                case Mark.O:
                    return "O";
            }
        }
        return " ";
    }

}