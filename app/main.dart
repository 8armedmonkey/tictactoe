import "package:tictactoe/tictactoe.dart";
import "presenter.dart";

main() {
    final board = Board(Turn(Mark.X));
    final presenter = Presenter();

    presenter.render(board);

    while (board.getStatus().isOngoing()) {
        try {
            final int cell = presenter.acceptInput(board.getCurrentTurn());
            board.select(cell);
        } catch (e) {
            presenter.presentError(e);
        }

        presenter.render(board);
    }

    presenter.presentWinner(board.getStatus().getWinner());
}